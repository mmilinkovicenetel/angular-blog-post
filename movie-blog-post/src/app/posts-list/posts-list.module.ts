import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostsListComponent } from './posts-list.component';
import {RouterModule} from "@angular/router";



@NgModule({
  declarations: [PostsListComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [PostsListComponent]
})
export class PostsListModule { }
