import { Component, OnInit } from '@angular/core';
import {Post} from "../../Services/Clasess/Post";
import {PostsService} from "../../Services/posts.service";

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.css']
})
export class PostsListComponent implements OnInit {
  public posts: Post[];

  constructor(private postsService: PostsService) { }

  ngOnInit(): void {
    this.postsService.getPosts().subscribe({
      next: posts => this.posts = posts
    });
  }

}
