import { Component, OnInit } from '@angular/core';
import {PostsService} from "../../Services/posts.service";
import {Post} from "../../Services/Clasess/Post";

@Component({
  selector: 'app-highlight',
  templateUrl: './highlight.component.html',
  styleUrls: ['./highlight.component.css']
})
export class HighlightComponent implements OnInit {

  public posts: Post[];

  constructor(private postsService: PostsService) { }

  ngOnInit(): void {
    let items = this.postsService.getPosts();

    items.subscribe({
        next: posts => this.posts = posts.filter(post => post.highlighted)
    });
  }
}
