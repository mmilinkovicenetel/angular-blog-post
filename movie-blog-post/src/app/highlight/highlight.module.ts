import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HighlightComponent } from './highlight.component';
import {RouterModule} from "@angular/router";



@NgModule({
  declarations: [HighlightComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [HighlightComponent]
})
export class HighlightModule { }
