import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import {HighlightModule} from "../highlight/highlight.module";
import {TopPostModule} from "../top-post/top-post.module";
import {NewsletterModule} from "../newsletter/newsletter.module";

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    HighlightModule,
    TopPostModule,
    NewsletterModule
  ]
})
export class HomeModule { }
