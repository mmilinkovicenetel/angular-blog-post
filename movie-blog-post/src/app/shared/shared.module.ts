import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopbarComponent } from './topbar/topbar.component';
import { MenuComponent } from './menu/menu.component';
import { SliderComponent } from './slider/slider.component';
import { FooterComponent } from './footer/footer.component';
import { FoobarComponent } from './foobar/foobar.component';
import {RouterModule} from "@angular/router";



@NgModule({
  declarations: [TopbarComponent, MenuComponent, SliderComponent, FooterComponent, FoobarComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    TopbarComponent,
    MenuComponent,
    SliderComponent,
    FooterComponent,
    FoobarComponent
  ]
})
export class SharedModule { }
