import { Component, OnInit } from '@angular/core';
import {MenusService} from "../../../Services/menus.service";
import {IMenu} from "../../../Services/Interfaces/IMenu";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  public title: string = "Movie Blog Post";
  public menuItems: IMenu[];
  public errorMessage: string;

  public constructor(private menusService: MenusService) { }

  public ngOnInit(): void {
      //return type is Observable, so we must to subscribe, before we use data.
      let items = this.menusService.getMenuItems();

      items.subscribe({
        next: menuItems => {this.menuItems = menuItems;},
        error: err => {this.errorMessage = err;}
      });
  }
}
