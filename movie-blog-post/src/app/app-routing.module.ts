import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeModule} from "./home/home.module";
import {HomeComponent} from "./home/home.component";
import {SinglePostModule} from "./single-post/single-post.module";
import {SinglePostComponent} from "./single-post/single-post.component";
import {PostsListModule} from "./posts-list/posts-list.module";
import {PostsListComponent} from "./posts-list/posts-list.component";
import {CreatePostComponent} from "./create-post/create-post.component";

const routes: Routes = [
  {path: "", component: HomeComponent},
  {path: "home", component: HomeComponent},
  {path: "posts", component: PostsListComponent},
  {path: "post-detail/:id", component: SinglePostComponent},
  {path: "create-post", component: CreatePostComponent},
  {path: "**", redirectTo: "home"}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    HomeModule,
    SinglePostModule,
    PostsListModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
