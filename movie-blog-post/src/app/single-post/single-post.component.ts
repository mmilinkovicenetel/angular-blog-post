import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {PostsService} from "../../Services/posts.service";
import {Post} from "../../Services/Clasess/Post";
import {Comment} from "../../Services/Interfaces/IComment";
import {CommentsService} from "../../Services/comments.service";

@Component({
  selector: 'app-single-post',
  templateUrl: './single-post.component.html',
  styleUrls: ['./single-post.component.css']
})
export class SinglePostComponent implements OnInit {

  public id: number;
  public singlePost: Post;
  public comments: Comment[];

  constructor(private route: ActivatedRoute, private postsService: PostsService, private commentsService: CommentsService) { }

  ngOnInit(): void {
    this.id = +this.getRouteParam("id");
    this.getPost();
    this.getComments();
  }

  private getRouteParam(param: string): string
  {
    return this.route.snapshot.paramMap.get(param);
  }

  private getPost(): void {
    this.postsService.getPosts().subscribe({
      next: posts => {this.setSinglePost(posts)}
    });
  }

  private getComments(): void {
    this.commentsService.getComments().subscribe({
      next: comments => this.comments = comments.filter(singleComment => singleComment.postId == this.id)
    });
  }

  private setSinglePost(posts: Post[]) {
    this.singlePost = posts.filter(value => value.postId == this.id)[0];
  }
}
