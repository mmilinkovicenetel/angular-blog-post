import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopPostComponent } from './top-post.component';
import {RouterModule} from "@angular/router";



@NgModule({
  declarations: [TopPostComponent],
  exports: [
    TopPostComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class TopPostModule { }
