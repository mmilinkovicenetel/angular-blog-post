import { Component, OnInit } from '@angular/core';
import {PostsService} from "../../Services/posts.service";
import {Post} from "../../Services/Clasess/Post";

@Component({
  selector: 'app-top-post',
  templateUrl: './top-post.component.html',
  styleUrls: ['./top-post.component.css']
})
export class TopPostComponent implements OnInit {

  public posts: Post[];
  public title: string = "Top rated posts";
  public subTitle: string = "This is the list of best rated movies in past month"

  constructor(private postsService: PostsService) { }

  ngOnInit(): void {
    this.postsService.getPosts().subscribe({next: posts => this.posts = posts.sort((a,b) => b.rating - a.rating).slice(0,3)});
  }

}
