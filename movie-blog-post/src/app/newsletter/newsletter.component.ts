import { Component, OnInit } from '@angular/core';
import {PostsService} from "../../Services/posts.service";
import {Post} from "../../Services/Clasess/Post";

@Component({
  selector: 'app-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.css']
})
export class NewsletterComponent implements OnInit {

  public posts: Post[];
  public title: string = "Newest posts";
  public newsLetterTitle: string = "Newsletter";
  public desc: string = "Sed a eros ornare pulvinar lorem eget volutpat ex fusce sollicitudin porta erat ac dictum sed nec augue augue praesent congue.";
  public subDesc: string  = "Ullamcorper neque a bibendum dolor placerat non nulla facilisi nullam ac velit.";

  constructor(private postsService: PostsService) { }

  ngOnInit(): void {
    this.postsService.getPosts()
        .subscribe({
            next: posts => this.posts = posts.sort((a,b) => <any>new Date(a.date_posted) - <any>new Date(b.date_posted)).slice(0,3)
        });
  }
}
