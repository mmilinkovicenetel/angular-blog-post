import {Injectable} from "@angular/core";
import {MainService} from "./main.service";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {IComment} from "./Interfaces/IComment";
import {catchError, tap} from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class CommentsService extends MainService{

  public constructor(client: HttpClient) {
    super(client, "comments", "comments");
  }

  public getComments(): Observable<IComment[]> {
    return this.httpClient.get<IComment[]>(this.url).pipe(
      tap (x => console.log(x)),
      catchError (err => this.handleError(err))
    );
  }
}
