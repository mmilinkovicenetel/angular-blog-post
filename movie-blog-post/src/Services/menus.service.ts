import {Injectable} from "@angular/core";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {IMenu} from "./Interfaces/IMenu";
import {catchError, tap} from "rxjs/operators";
import {Observable, throwError} from "rxjs";
import {MainService} from "./main.service";

@Injectable({
  providedIn: "root"
})
export class MenusService extends MainService{

  public constructor(client: HttpClient) {
    super(client, "menus", "items");
  }

  public getMenuItems(): Observable<IMenu[]> {
    return this.httpClient.get<IMenu[]>(this.url)
      .pipe(
        tap (x => console.log(x)),
        catchError (err => this.handleError(err))
      );
  }
}
