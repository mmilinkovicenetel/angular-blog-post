import {Injectable} from "@angular/core";
import {MainService} from "./main.service";
import {HttpClient} from "@angular/common/http";
import {IPost} from "./Interfaces/IPost";
import {catchError, tap} from "rxjs/operators";
import {Observable} from "rxjs";
import {Post} from "./Clasess/Post";

@Injectable({
  providedIn: "root"
})
export class PostsService extends MainService{

  public constructor(client: HttpClient) {
    super(client, "posts", "posts");
  }

  public getPosts(): Observable<IPost[]> {
    return this.httpClient.get<IPost[]>(this.url)
      .pipe(
          tap(x => console.log(x)),
          catchError (err => this.handleError(err))
      );
  }
}
