export interface IMenu {
  menuId: number;
  name: string;
  path: string;
}
