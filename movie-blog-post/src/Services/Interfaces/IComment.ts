export interface IComment {
  commentId: number,
  postId: number,
  title: string,
  user_name: string,
  body: string,
  date_posted: string
}

export class Comment implements IComment{
  body: string;
  commentId: number;
  date_posted: string;
  postId: number;
  title: string;
  user_name: string;
}
