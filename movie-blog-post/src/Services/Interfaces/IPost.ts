export interface IPost {
  postId: number,
  title: string,
  short_desc: string,
  long_desc: string,
  main_image: string,
  first_image: string,
  second_image: string,
  rating: number,
  highlighted: boolean,
  date_posted: string
}
