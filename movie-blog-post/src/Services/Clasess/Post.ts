import {IPost} from "../Interfaces/IPost";

export class Post implements IPost {
  date_posted: string;
  first_image: string;
  highlighted: boolean;
  long_desc: string;
  main_image: string;
  postId: number;
  rating: number;
  second_image: string;
  short_desc: string;
  title: string;
}
